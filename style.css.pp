#lang pollen

@import url('/fonts/main.css');
@import url('tufte.css');

body {
    font-family: "Rac", "Open Sans Hebrew", "Alef";
    direction: rtl;
	◊;{
    position: relative; ◊; this establishes body as reference container 
    padding: 0;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    max-width: 1000px;
    min-width: 520px;
    font-size: 18pt;
	}
}

.wikilink:link { text-decoration: none;
         background: -webkit-linear-gradient(#fffff8, #fffff8), -webkit-linear-gradient(#fffff8, #fffff8), -webkit-linear-gradient(#333, #333);
         background: linear-gradient(#fffff8, #fffff8), linear-gradient(#fffff8, #fffff8), linear-gradient(#aaa, #aaa);
         -webkit-background-size: 0.05em 1px, 0.05em 1px, 1px 1px;
         -moz-background-size: 0.05em 1px, 0.05em 1px, 1px 1px;
         background-size: 0.05em 1px, 0.05em 1px, 1px 1px;
         background-repeat: no-repeat, no-repeat, repeat-x;
         text-shadow: 0.03em 0 #fffff8, -0.03em 0 #fffff8, 0 0.03em #fffff8, 0 -0.03em #fffff8, 0.06em 0 #fffff8, -0.06em 0 #fffff8, 0.09em 0 #fffff8, -0.09em 0 #fffff8, 0.12em 0 #fffff8, -0.12em 0 #fffff8, 0.15em 0 #fffff8, -0.15em 0 #fffff8;
         background-position: 0% 93%, 100% 93%, 0% 93%; }
@media screen and (-webkit-min-device-pixel-ratio: 0) { .wikilink:link { background-position-y: 87%, 87%, 87%; } }

p.subtitle {
	font-style: normal;
}

.latin {
	font-family: "Vesper";
	direction: ltr !important;
}

.gentium {
	font-family: "Gentium Plus", "Gentium";
	direction: ltr !important;
}

.highlight {
	border-right: 1px gray solid;
	padding-right: 1em;
	width: calc(55% - 1em - 1px);
}

.todo {
	color: #E11;
}


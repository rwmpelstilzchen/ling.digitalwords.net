#lang pollen

◊(require css-tools/font-face)

◊(ffd/rp "Vesper" "/fonts/vesper/regular.ttf" 
#:font-style "normal" #:font-weight "normal")

◊(ffd/rp "Vesper" "/fonts/vesper/regular-italic.ttf" 
#:font-style "italic" #:font-weight "normal")

◊(ffd/rp "Vesper" "/fonts/vesper/bold.ttf" 
#:font-style "normal" #:font-weight "bold")

◊(ffd/rp "Vesper" "/fonts/vesper/bold-italic.ttf" 
#:font-style "italic" #:font-weight "bold")

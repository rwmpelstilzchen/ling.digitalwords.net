#lang pollen

◊(require css-tools/font-face)

◊(ffd/rp "Gentium" "/fonts/gentium/regular.woff" 
#:font-style "normal" #:font-weight "normal")

◊(ffd/rp "Gentium" "/fonts/vesper/regular-italic.woff" 
#:font-style "italic" #:font-weight "normal")

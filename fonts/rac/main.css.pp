#lang pollen

◊(require css-tools/font-face)

◊(ffd/rp "Rac-light" "/fonts/rac/light.woff" 
#:font-style "normal" #:font-weight "normal" #:base64 #f)

◊(ffd/rp "Rac-light" "/fonts/rac/medium.woff" 
#:font-style "normal" #:font-weight "bold" #:base64 #f)

◊(ffd/rp "Rac" "/fonts/rac/regular.woff" 
#:font-style "normal" #:font-weight "normal" #:base64 #f)

◊(ffd/rp "Rac" "/fonts/rac/bold.woff" 
#:font-style "normal" #:font-weight "bold" #:base64 #f)

◊;{
◊(ffd/rp "rac" "/fonts/rac/heavy.woff" 
#:font-style "normal" #:font-weight "bolder" #:base64 #t)
}
